import presentacion.Modelo;

public class DesktopApplication {
		
    private Modelo miApp;
    
    public static void main(String[] args) {
        new DesktopApplication();
    }

    public DesktopApplication() {
        miApp = new Modelo();
        miApp.iniciar();
    }
}
