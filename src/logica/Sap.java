package logica;

import java.util.HashMap;
import java.util.Map;

public class Sap {
	
	/**
	 * Atributos SPA
	 * */
	private Integer intrucciones;
	private Integer paso;
	private boolean acarreo;
	private boolean cero;
	private Map<String, Integer> palabraControl;
	
	
	public boolean isCero() {
		return cero;
	}
	public void setCero(boolean cero) {
		this.cero = cero;
	}
	public Map<String, Integer> getPalabraControl() {
		return palabraControl;
	}
	public void setPalabraControl(Map<String, Integer> palabraControl) {
		this.palabraControl = palabraControl;
	}
	public int getIntrucciones() {
		return intrucciones;
	}
	public void setIntrucciones(int intrucciones) {
		this.intrucciones = intrucciones;
	}
	public int getPaso() {
		return paso;
	}
	public void setPaso(int paso) {
		this.paso = paso;
	}
	public boolean isAcarreo() {
		return acarreo;
	}
	public void setAcarreo(boolean acarreo) {
		this.acarreo = acarreo;
	}
		
	@Override
	public String toString() {
		return "Sap [intrucciones=" + intrucciones + ", paso=" + paso + ", acarreo=" + acarreo + ", cero=" + cero
				+ ", palabraControl=" + palabraControl + "]";
	}
	
	/**
	 * Metodo fetch siempre se debe llamar al inicio cualquier compilacion de codigo
	 * */
	public void fetch(int paso) {
		this.intrucciones = null;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
				
		switch (paso) {
		case 0:
			this.palabraControl.put("MI", 1);
			this.palabraControl.put("CO", 1);
			print("fectch - Paso " + paso);
			print(this.toString());
			break;

		case 1:
			this.palabraControl.put("RO", 1);
			this.palabraControl.put("II", 1);
			this.palabraControl.put("CE", 1);
			print("fectch - Paso " + paso);
			print(this.toString());
			break;
		}
	}
	
	
	/**
	 * Cargar A
	 * 
	 * Coloca en el registro A, el valor que se encuentra
	 * almacenado en la posición X de memoria
	 * */
	public void lda(int paso) {
		this.intrucciones = 1;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("MI", 1);
			this.palabraControl.put("IO", 1);
			print("lda - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			this.palabraControl.put("RO", 1);
			this.palabraControl.put("AI", 1);
			print("lda - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("lda - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	
	/**
	 * Suma
	 * 
	 * Coloca en el registro A, el resultado de la suma
	 * entre lo anteriormente almacenado en A con lo que
	 * se encuentra almacenado en la posición X de	
	 * memoria
	 * */
	public void add(int paso) {
		this.intrucciones = 2;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("MI", 1);
			this.palabraControl.put("IO", 1);
			print("add - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			this.palabraControl.put("RO", 1);
			this.palabraControl.put("BI", 1);
			print("add - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			this.palabraControl.put("AI", 1);
			this.palabraControl.put("EO", 1);
			this.palabraControl.put("FI", 1);
			print("add - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Resta
	 * 
	 	Coloca en el registro A, el resultado de la resta
		entre lo anteriormente almacenado en A con lo que
		se encuentra almacenado en la posición X de
		memoria
	 * */
	public void sub(int paso) {
		this.intrucciones = 3;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("MI", 1);
			this.palabraControl.put("IO", 1);
			print("sub - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			this.palabraControl.put("RO", 1);
			this.palabraControl.put("BI", 1);
			print("sub - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			this.palabraControl.put("AI", 1);
			this.palabraControl.put("EO", 1);
			this.palabraControl.put("SU", 1);
			this.palabraControl.put("FI", 1);
			print("sub - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Almacenar
	 * 
		Coloca en la posición X de memoria, lo que se
		encuentra en el registro A
	 * */
	public void sta(int paso) {
		this.intrucciones = 4;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("MI", 1);
			this.palabraControl.put("IO", 1);
			print("sta - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			this.palabraControl.put("RI", 1);
			this.palabraControl.put("AO", 1);
			print("sta - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("sta - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Carga inmediata
	 * 
		Coloca en el registro A, un número especificado
		(4bits)
	 * */
	public void ldi(int paso) {
		this.intrucciones = 5;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("IO", 1);
			this.palabraControl.put("AI", 1);
			print("ldi - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("ldi - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("ldi - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Salto incondicional
	 * 
		Salta a la posición X de memoria
	 * */
	public void jmp(int paso) {
		this.intrucciones = 6;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("IO", 1);
			this.palabraControl.put("J", 1);
			print("jmp - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("jmp - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("jmp - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Salto si hay acarreo
	 * 
		Salta a la posición X de memoria
	 * */
	public void jc(int paso) {
		this.intrucciones = 7;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("IO", 1);
			this.palabraControl.put("J", 1);
			print("jc - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("jc - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("jc - Paso" +  paso);
			print(this.toString());
			break;
		}	
		
	}
	
	/**
	 * Saldo si cero
	 * 
		Salta a la posición X de memoria
	 * */
	public void jz(int paso) {
		this.intrucciones = 7;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("IO", 1);
			this.palabraControl.put("J", 1);
			print("jz - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("jz - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("jz - Paso" +  paso);
			print(this.toString());
			break;
		}	
		
	}
	
	/**
	 * Visualizar
	 * 
	 * Muestra en el display
	 * */
	public void out(int paso) {
		this.intrucciones = 14;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("AO", 1);
			this.palabraControl.put("OI", 1);
			print("add - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("add - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("add - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	/**
	 * Parar
	 * 
	 * Detiene el procesador
	 * */
	public void hlt(int paso) {
		this.intrucciones = 15;
		this.acarreo  = false;
		this.palabraControl = new HashMap<>();
		
		switch (paso) {
		case 2:
			this.palabraControl.put("HLT", 1);
			print("add - Paso " +  paso);
			print(this.toString());
			break;

		case 3:
			print("add - Paso " +  paso);
			print(this.toString());
			break;
			
		case 4:
			print("add - Paso" +  paso);
			print(this.toString());
			break;
		}
	}
	
	private void print(Object x) {
		//print(x);
	}
	
	
}
