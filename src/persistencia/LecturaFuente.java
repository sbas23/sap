package persistencia;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LecturaFuente {

	public List<Map<String, Object>> lecturaArchivo() throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader("src/resources/codigo.txt"));
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		    
		    List<Map<String, Object>> comandos = new ArrayList<>();
		    String[] lineaConSalto = everything.split("\n");
		    
		    for (int i = 0; i < lineaConSalto.length; i++) {
		    	String[] linea = lineaConSalto[i].split(";");
		    	Map<String, Object> lineaMap = new HashMap<>();
		    	if (linea.length > 1 ) {
		    		lineaMap.put("posicion", linea[0]);
		    		lineaMap.put("instruccion", linea[1]);
		    		if (linea.length == 3) {
		    			lineaMap.put("valor", linea[2]);
					}
		    		comandos.add(lineaMap);
				} else {
					lineaMap.put("posicion", linea[0]);
					comandos.add(lineaMap);
				}
			}
		    return comandos;
		    
		} finally {
		    br.close();
		}
	}
}
