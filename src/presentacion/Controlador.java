package presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Controlador implements ActionListener {

    private final VentanaUno ventana;

    public Controlador(VentanaUno aThis) {
        ventana = aThis;        
    }
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ventana.getMiModelo().iniciarAnimacion();
	}

}
