package presentacion;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import logica.Sap;
import persistencia.LecturaFuente;

public class Modelo implements Runnable {

	private static final String INSTRUCCION = "instruccion";
	private static final String VALOR = "valor";
	private static final String POSICION = "posicion";
	
	private Sap sap = new Sap();
	private VentanaUno ventana;
	private Thread hiloDibujo;
	
    public VentanaUno getVentana() {
        if(ventana == null){
            ventana = new VentanaUno(this);
        }
        return ventana;
    }
	
    public void iniciar(){
   		getVentana().setVisible(true);
    }
    
    public void iniciarAnimacion() {
        getVentana().getStartButton().setEnabled(false);
        hiloDibujo = new Thread(this);
        hiloDibujo.start();
    }
    
	
	public void ejecutar() throws IOException, InterruptedException {
		LecturaFuente lecturaFuente = new LecturaFuente();
		List<Map<String, Object>> comandos = lecturaFuente.lecturaArchivo();
		
		if (comandos != null && !comandos.isEmpty()) {
			
			int acumulador = 0;
			int posicionMemoria = 0;
			int sta = 0;
			int velocidad = 500;
			
			sap.fetch(0);
			getVentana().getLcdLabel().setText("0");
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
			Thread.sleep(velocidad);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
			Thread.sleep(velocidad);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
			Thread.sleep(velocidad);
			
			sap.fetch(1);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
			Thread.sleep(velocidad);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
			Thread.sleep(velocidad);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
			getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
			Thread.sleep(velocidad);
			getVentana().getpCLabel().setText("");
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
			Thread.sleep(velocidad);
			getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
			Thread.sleep(velocidad);
			
			
			for (int posicionCodigo = 0; posicionCodigo < comandos.size(); posicionCodigo++) {
				
								
				switch (comandos.get(posicionCodigo).get(INSTRUCCION).toString()) {
				case "LDA":
					
					posicionMemoria = Integer.parseInt(comandos.get(posicionCodigo).get(VALOR).toString());
					for (Map<String, Object> inst : comandos) {
						if (Integer.parseInt(inst.get(POSICION).toString()) == posicionMemoria) {
							acumulador = Integer.parseInt(inst.get(INSTRUCCION).toString());
						}
					}
					
					for (int paso = 2; paso < 5; paso++) {
						
						sap.lda(paso);
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 3) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
						}
						
					}
					
					break;
					
				case "ADD":
					
					posicionMemoria = Integer.parseInt(comandos.get(posicionCodigo).get(VALOR).toString());
					for (Map<String, Object> inst : comandos) {
						if (Integer.parseInt(inst.get(POSICION).toString()) == posicionMemoria) {
							acumulador = acumulador + Integer.parseInt(inst.get(INSTRUCCION).toString());
						}
					}
					
					for (int paso = 2; paso < 5; paso++) {
						sap.add(paso);
						
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 3) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/registroB.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 4) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/alu.jpg"));
							Thread.sleep(velocidad);
						}
					}
					break;
					
				case "SUB":
					
					posicionMemoria = Integer.parseInt(comandos.get(posicionCodigo).get(VALOR).toString());
					for (Map<String, Object> inst : comandos) {
						if (Integer.parseInt(inst.get(POSICION).toString()) == posicionMemoria) {
							acumulador = acumulador - Integer.parseInt(inst.get(INSTRUCCION).toString());
						}
					}
									
					for (int paso = 2; paso < 5; paso++) {
						sap.sub(paso);
						
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 3) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/registroB.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 4) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/alu.jpg"));
							Thread.sleep(velocidad);
						}
						
					}
					
					break;
					
				case "STA":
					
					sta = acumulador;
					
					for (int paso = 2; paso < 5; paso++) {
						sap.sta(paso);
						
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/mar+ram.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
						
						if (paso == 3) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/ram-ri.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
						
						
					}
					break;
					
				case "LDI":
					
					acumulador = Integer.parseInt(comandos.get(posicionCodigo).get(VALOR).toString());
					
					for (int paso = 2; paso < 5; paso++) {
						sap.ldi(paso);
						
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
						}
					}
					break;
					
				case "JMP":
					posicionCodigo = Integer.parseInt(comandos.get(posicionCodigo).get(VALOR).toString())-1;
					
					for (int paso = 2; paso < 5; paso++) {
						sap.jmp(paso);
						
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
					}
					break;
					
				case "JC":
					for (int paso = 2; paso < 5; paso++) {
						sap.jc(paso);
						
						if (paso == 2 && acumulador > 9) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
					}
					break;
					
				case "JZ":
					for (int paso = 2; paso < 5; paso++) {
						sap.jz(paso);

						if (paso == 2 && acumulador == 0) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/instruccion.jpg"));
							getVentana().getpCLabel().setText(sap.getPalabraControl().toString());
							Thread.sleep(velocidad);
							getVentana().getpCLabel().setText("");
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/pc-clr.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
						}
					}
					break;
					
				case "OUT":
					System.out.println(new StringBuilder("Resultado: ").append(acumulador).toString());
					
					for (int paso = 2; paso < 5; paso++) {
						
						sap.out(paso);
						if (paso == 2) {
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/acum.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/bus.jpg"));
							Thread.sleep(velocidad);
							getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/salida.jpg"));
							getVentana().getLcdLabel().setText(String.valueOf(acumulador));
							Thread.sleep(velocidad*3);
						}
						
					}
					break;
					
				case "HLT":
					posicionCodigo = comandos.size();
					for (int paso = 2; paso < 5; paso++) {
						sap.hlt(paso);
						getVentana().getImagesLabel().setIcon(new ImageIcon("src/resources/images/sap_00.jpg"));
					}
					break;
				}
			}
			
		}

	}
	
	@Override
	public void run() {
        try {
            ejecutar();
      } catch (Exception ex) {
            JOptionPane.showMessageDialog(getVentana(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
        getVentana().getStartButton().setEnabled(true);
		
	}
	
	public void cambioImagen(String ruta) {
		getVentana().getImagesLabel().setIcon(new ImageIcon(ruta));
	}
}
