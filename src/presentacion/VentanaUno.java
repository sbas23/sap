package presentacion;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

public class VentanaUno extends JFrame {

	private static final long serialVersionUID = -7135175368615005091L;
	
	private Modelo miModelo;
    private Controlador control;
	
	JButton startButton;
	JLabel imagesLabel;
	JLabel pCLabel;
	JLabel lcdLabel;
	private final JLabel lblSap = new JLabel("SAP");	

	
	public VentanaUno(Modelo aThis) {
		miModelo = aThis;
		initialize();
        capturaEventos();
	}
	
    public Controlador getControl() {
        if(control == null){
            control = new Controlador(this);
        }
        return control;
    }
    
    public Modelo getMiModelo() {
        return miModelo;
    }
	
	public JButton getStartButton() {
		return startButton;
	}

	public JLabel getImagesLabel() {
		return imagesLabel;
	}
	
	public JLabel getpCLabel() {
		return pCLabel;
	}

	public JLabel getLcdLabel() {
		return lcdLabel;
	}
	
	private void initialize() {
		setBounds(100, 100, 629, 780);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		startButton = new JButton("Start");
		startButton.setBounds(520, 700, 85, 25);
		getContentPane().add(startButton);
		
		imagesLabel = new JLabel("");
		imagesLabel.setBounds(63, 12, 500, 650);
		imagesLabel.setIcon(new ImageIcon("src/resources/images/sap_00.jpg"));
		getContentPane().add(imagesLabel);
		
		imagesLabel.setBackground(Color.WHITE);
		imagesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblSap.setFont(new Font("Dialog", Font.BOLD, 16));
		lblSap.setHorizontalAlignment(SwingConstants.CENTER);
		lblSap.setBounds(12, 30, 607, 19);
		getContentPane().add(lblSap);
		
		pCLabel = new JLabel("");
		pCLabel.setHorizontalAlignment(SwingConstants.CENTER);
		pCLabel.setBounds(70, 647, 300, 15);
		getContentPane().add(pCLabel);
		
		lcdLabel = new JLabel("0");
		lcdLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lcdLabel.setBounds(410, 647, 70, 15);
		getContentPane().add(lcdLabel);
	}

	
    private void capturaEventos() {
    	 startButton.addActionListener(getControl());
    }
}
